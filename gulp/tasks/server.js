var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var config = require('../config');

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server : {
            baseDir : "./" + config.dest.root
        },
        files : [
            config.dest.root + '/*.html',
            config.dest.css + '/*.css',
            config.dest.img + '/**/*'
        ],
        port : config.serverPort
    });
});

module.exports = browserSync;