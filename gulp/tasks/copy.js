var gulp = require('gulp');
var config = require('../config.js');

gulp.task('copy:fonts', function() {
  return gulp
  .src([
    config.src.fonts + '/*.{ttf,eot,woff,woff2}',
    config.src.fonts + '/**/*.{ttf,eot,woff,woff2}'
  ])
  .pipe(gulp.dest(config.dest.fonts));
});

gulp.task('copy:node_lj', function() {
  return gulp
  .src([
    config.node_modules + '/jquery/dist/jquery.min.js',
    config.node_modules + '/jquery/dist/jquery.min.map',
    config.node_modules + '/jquery.cookie/jquery.cookie.js',
    config.node_modules + '/popper.js/dist/umd/popper.min.js',
    config.node_modules + '/popper.js/dist/umd/popper.min.js.map',
    config.node_modules + '/bootstrap/dist/js/bootstrap.min.js',
    config.node_modules + '/bootstrap/dist/js/bootstrap.min.js.map',
    config.node_modules + '/chart.js/dist/Chart.min.js',
    config.node_modules + '/select2/dist/js/select2.min.js',
    config.node_modules + '/moment/min/moment.min.js',
    config.node_modules + '/daterangepicker/daterangepicker.js',
    config.node_modules + '/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
  ])
  .pipe(gulp.dest(config.dest.node_libs_js));
});

gulp.task('copy:node_css', function() {
  return gulp
  .src([
    config.node_modules + '/bootstrap/dist/css/bootstrap.min.css',
    config.node_modules + '/bootstrap/dist/css/bootstrap.min.css.map',
    config.node_modules + '/select2/dist/css/select2.min.css',
    config.node_modules + '/daterangepicker/daterangepicker.css',
    config.node_modules + '/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
  ])
  .pipe(gulp.dest(config.dest.node_libs_css));
});

gulp.task('copy:lib', function() {
  return gulp
  .src(config.src.lib + '/**/*.*')
  .pipe(gulp.dest(config.dest.lib));
});

gulp.task('copy:icon', function() {
  return gulp
  .src(config.src.icons + '/**/*.{jpg,png,jpeg,svg,gif}')
  .pipe(gulp.dest(config.dest.icons));
});

gulp.task('copy:img', function() {
  return gulp
  .src([
    config.src.img + '/**/*.{jpg,png,jpeg,svg,gif}',
    '!' + config.src.img + '/svgo/**/*.*'
  ])
  .pipe(gulp.dest(config.dest.img));
});

gulp.task('copy:static', function() {
  return gulp.src(config.src.static + '/**/*')
  .pipe(gulp.dest(config.dest.root));
});

gulp.task('copy', [
  'copy:node_lj',
  'copy:node_css',
  'copy:lib',
  'copy:img',
  'copy:icon',
  'copy:fonts',
  'copy:static'
]);

gulp.task('copy:watch', function() {
  gulp.watch(config.src.img + '/*', ['copy']);
});
